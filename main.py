import os

from kivy.lang import Builder

from q_extraction import ImageProcessor, AskGPT

from kivymd.app import MDApp
from kivymd.uix.filemanager import MDFileManager
from kivymd.toast import toast

from kivymd.uix.screenmanager import ScreenManager
from kivy.uix.screenmanager import Screen
from kivymd.uix.label import MDLabel
from kivymd.uix.scrollview import ScrollView

from kivymd.uix.boxlayout import BoxLayout

from kivymd.uix.button import MDRaisedButton
from kivy.clock import Clock
import threading


class OCR:
    def __init__(self, path):
        self.path = path
        self.gpt_key_path = 'gpt_key.json'
        self.google_key_path = 'google_key.json'

    def ocr(self):
        image_processor = ImageProcessor(
            self.path, self.gpt_key_path, self.google_key_path)
        result = image_processor.extract_questions()
        print(f'cale wyjscie OCR: {result}')

        return result


class GPT:
    def __init__(self) -> None:
        self.gpt_key_path = 'gpt_key.json'
        self.ask_gpt = AskGPT(self.gpt_key_path)


class OCRScreen(Screen):
    pass


class LoadingScreen(Screen):
    pass


class BaseScreen(Screen):
    def display_ocr(self, questions, answers, iterate=True):
        self.manager.current = 'ocr'
        ocr_screen = self.manager.get_screen('ocr')

        scroll_view = ScrollView()
        box_layout = BoxLayout(orientation='vertical', size_hint_y=None)
        box_layout.bind(minimum_height=box_layout.setter('height'))

        if iterate:
            for question, answer in zip(questions, answers):
                self.add_labels(box_layout, question, answer)
        else:
            self.add_labels(box_layout, questions, answers)

        scroll_view.add_widget(box_layout)
        ocr_screen.add_widget(scroll_view)
        button = MDRaisedButton(text='BACK', size_hint=(
            1, 0.1), md_bg_color="orange", on_release=self.back_to_filemanager)
        box_layout.add_widget(button)

        return ocr_screen
    
    def add_labels(self, layout, question, answer):
        question_label = MDLabel(
            text=f"{question}",
            halign="left",
            adaptive_height=True,
        )

        answer_label = MDLabel(
            text=f"{answer}\n",
            halign="left",
            theme_text_color="Custom",
            text_color="green",
            adaptive_height=True,
        )
        layout.add_widget(question_label)
        layout.add_widget(answer_label)

    def clear_ocr_screen(self):
        ocr_screen = self.manager.get_screen('ocr')
        ocr_screen.clear_widgets()

    def back_to_filemanager(self, *args):
        self.clear_ocr_screen()
        self.manager.current = 'filemanager'


class AskQuestion(BaseScreen):
    def on_send_button(self, text):
        self.manager.current = 'loading'

        # THREAD
        def worker():
            questions, answers = self.ask_gpt(text)
            # Schedule UI update on the main thread
            Clock.schedule_once(
                lambda dt: self.display_ocr(questions, answers, iterate=False), 0)

        threading.Thread(target=worker).start()

    def ask_gpt(self, text):
        gpt = GPT()
        return gpt.ask_gpt.gpt_response(text)


class FileManager(BaseScreen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.manager_open = False
        self.file_manager = MDFileManager(
            exit_manager=self.exit_manager, select_path=self.select_path
        )

    def file_manager_open(self):
        self.file_manager.show(os.path.expanduser("~"))
        self.manager_open = True

    def select_path(self, path: str):
        '''
        It will be called when you click on the file name
        or the catalog selection button.
        '''

        self.exit_manager()
        toast(path)

        self.manager.current = 'loading'

        # THREAD
        def worker():
            questions, answers = self.questions_extract(path)
            # Schedule UI update on the main thread
            Clock.schedule_once(
                lambda dt: self.display_ocr(questions, answers), 0)

        threading.Thread(target=worker).start()

    def questions_extract(self, path):
        ocr = OCR(path)
        result_label = ocr.ocr()
        return result_label['questions'], result_label['answers']

    def exit_manager(self, *args):
        '''Called when the user reaches the root of the directory tree.'''

        self.manager_open = False
        self.file_manager.close()

    def events(self, instance, keyboard, keycode, text, modifiers):
        '''Called when buttons are pressed on the mobile device.'''

        if keyboard in (1001, 27):
            if self.manager_open:
                self.file_manager.back()
        return True


class Example(MDApp):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self):
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "Orange"
        Builder.load_file('ui.kv')

        # Create managers
        filemanager = FileManager(name='filemanager')
        loading = LoadingScreen(name='loading')
        ocr = OCRScreen(name='ocr')
        askquestion = AskQuestion(name='askquestion')
        screenmanager = ScreenManager()

        # Create widgets
        screenmanager.add_widget(filemanager)
        screenmanager.add_widget(loading)
        screenmanager.add_widget(ocr)
        screenmanager.add_widget(askquestion)

        return screenmanager


Example().run()
